# README #

Projto Feito com php orientado a objeto.

## Estrutura do projeto ##

 Summary of set up
* ROOT
    * app
        *  bootstrap
        * config
        * lib
        * models
        * views
    * public
    * global.php
    * index.php

#### app ####
Pasa principal da aplicação
#### app/bootstrap ####
Contém o arquivo app.php que é responsável pelo autoload de todas as clases que serão utilizadas no sistema

#### app/config ####
Possui a classe Config, a mesma tem como função definir as configurações do systema.
Como por exemplo onde fica as views e layouts

#### app/lib ####
Nessa pasta ficam as bibliotecas que foram criadas para manipulação de banco de dados e shh.

#### app/models ####
Nessa pasta ficam as models, as mesmas herdam a biblioteca de conexão com o banco de dados

#### app/models ####
Nessa pasta ficam as models, as mesmas herdam a biblioteca de conexão com o banco de dados

#### app/views ####
Nesse caminho se localizam as views, componentes e layouts do sistema

#### public ####
Arquivos públicos como css e javascript

#### global.php ####
Todas as contantes que serão utilizadas no sistema

#### index.php ####
Core do sistema.

### Design Patters ###
* Projeto feito com PHP orientado a objetos
* Todas as Classes de conexão foram feitas utilizando singleton
*  Para todas as Classes de conexão foi criado uma factory
* O arquivo index.php, na raiz, trabalha como front controller