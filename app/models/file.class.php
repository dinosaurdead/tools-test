<?php
/**
 * Classe de conexão com o bancoo PDO no padrão Singleton.
 * Modo de Usar:
 * require_once './Database.class.php';
 * $db = Database::conexao();
 * E agora use as funções do PDO (prepare, query, exec) em cima da variável $db.
 */
class File extends Database
{
    public static function save($name, $hash)
    {
    	$sql = 'INSERT INTO files (`name`, `hash`) VALUES (:name, :hash)';

    	$pSql = self::conn()->prepare($sql);

        $pSql->bindValue(":name", $name);
        $pSql->bindValue(":hash", $hash);

        return $pSql->execute();
	}

	public static function getList()
    {
    	$sql = 'SELECT * FROM files';

    	$pSql = self::conn()->prepare($sql);

        $pSql->execute();
        
        return $pSql->fetchAll();
	}

	public static function get($id)
    {
    	$sql = 'SELECT * FROM files WHERE id = :id';

    	$pSql = self::conn()->prepare($sql);
    	$pSql->bindValue(":id", $id);

        $pSql->execute();
        
        return $pSql->fetchAll();
	}

	public static function getHash($id)
    {
    	$sql = 'SELECT hash FROM files WHERE id = :id LIMIT 1';

    	$pSql = self::conn()->prepare($sql);
    	$pSql->bindValue(":id", $id);

        $pSql->execute();
        
        return $pSql->fetch(PDO::FETCH_COLUMN, 0);
	}

}