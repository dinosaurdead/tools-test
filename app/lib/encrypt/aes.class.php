<?php

class Aes
{
    protected static $key = '';

    protected static $instance = '';

    protected static $salt  = '';
    
    private function __construct()
    {
        self::$key  = hash('SHA256',
                pack('H*', "00112233445566778899aabbccddeeff00112233445566778899aabbccddeeff00"),
                true
        );

         self::$salt = hash('MD5', substr(md5(rand()), 0, 32), true);
    }
    
    public static function aes256CbcEncrypt($data) {
        self::run();

        $padding = 16 - (strlen($data) % 16);
        $data .= str_repeat(chr($padding), $padding);
        return [
                'text' => base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, self::$key, $data, MCRYPT_MODE_CBC, self::$salt)),
                'salt' => base64_encode(self::$salt),
                'hash' => base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, self::$key, $data, MCRYPT_MODE_CBC, self::$salt) . "|hash|" . self::$salt)
                ];
    }

    public static function aes256CbcDecrypt($data, $salt) {
        self::run();

        $data = base64_decode($data);
        $salt = base64_decode($salt);
        $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, self::$key, $data, MCRYPT_MODE_CBC, $salt);
        $padding = ord($data[strlen($data) - 1]);
        return substr($data, 0, -$padding);
    }

    public static function aes256CbcDecryptHash($hash) {
        self::run();
        $data = explode("|hash|", base64_decode($hash)) ;
        $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, self::$key, $data[0], MCRYPT_MODE_CBC, $data[1]);
        $padding = ord($data[strlen($data) - 1]);
        return substr($data, 0, -$padding);
    }

    # Método estático - acessível sem instanciação.
    public static function run()
    {
        # Garante uma única instância. Se não existe uma conexão, criamos uma nova.
        if (!self::$instance)
        {
            new Aes();
        }

        # Retorna a conexão.
        return self::$instance;
    }
}