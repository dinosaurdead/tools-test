<?php
/**
 * Classe de conexão com o bancoo PDO no padrão Singleton.
 * Modo de Usar:
 * require_once './Database.class.php';
 * $db = Database::conexao();
 * E agora use as funções do PDO (prepare, query, exec) em cima da variável $db.
 */
class Database
{
    protected static $db;

    private function __construct()
    {
        $dbHost = "localhost";
        $dbName = "tools_test";
        $dbUser = "root";
        $dbPass = "test";
        $dbDriver = "mysql";

        try
        {
            # Atribui o objeto PDO à variável $db.
            self::$db = new PDO("$dbDriver:host=$dbHost; dbname=$dbName", $dbUser, $dbPass);
            # Garante que o PDO lance exceções durante erros.
            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            # Garante que os dados sejam armazenados com codificação UFT-8.
            self::$db->exec('SET NAMES utf8');
        }
        catch (PDOException $e)
        {
            die("Connection Error: " . $e->getMessage());
        }
    }

    # Método estático - acessível sem instanciação.
    public static function conn()
    {
        # Garante uma única instância. Se não existe uma conexão, criamos uma nova.
        if (!self::$db)
        {
            new Database();
        }

        # Retorna a conexão.
        return self::$db;
    }

}