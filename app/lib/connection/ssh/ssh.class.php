<?php

class Ssh
{
    protected static $conn;
    protected  $ip;
    protected  $port;
    protected  $user;
    protected  $pass;

    private function __construct($ip, $user, $pass, $port)
    {
    	$this->ip = $ip;
    	$this->$port = $port;
    	$this->user = $user;
    	$this->pass = $pass;

        try
        {
        	$ssh = ssh2_connect($this->ip, $this->port);

        	if(!$ssh) { 
        		die('ip or port invalid');
        	}

        	if (!@ssh2_auth_password($ssh, $this->user, $this->pass)) {
			    die('user or pass invalid');
			}
        }
        catch (PDOException $e)
        {
            die("Connection Error: " . $e->getMessage());
        }
    }

    // static method access without instance 
    public static function conn($ip, $user, $pass, $port = 22)
    {
        
        if (!self::$conn)
        {
            new Ssh($ip, $user, $pass, $port);
        }

        //return connecion
        return self::$conn;
    }

    
    public function exec($command)
    {
        $stream = ssh2_exec(self::$conn, $command);
    }

}