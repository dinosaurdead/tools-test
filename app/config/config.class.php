<?php

class Config
{
    protected static $viewPath = 'app/views/layout/default.php';
    protected static  $layoutPath = 'app/views/layout/default.php';
    
    public static function viewPath()
    {
        return self::$viewPath;
    }

    public static function layoutPath()
    {
        return self::$layoutPath;
    }
}