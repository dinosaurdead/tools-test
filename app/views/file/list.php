<?php
$files = File::getList();

if(count($files) > 0) {
?>
  <div class="container">
    <h2>Files</h2>
    <p>Files added in system</p>
    <form action="/file-comparator" method="POST" enctype="multipart/form-data">
      <table class="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Select</th>
          </tr>
        </thead>
        <tbody>
          <?php for($i = 0; $i < count($files); $i++) {?>
            <tr>
              <td>
                <?php echo $files[$i]['id']; ?>
              </td>
              <td>
                <?php echo $files[$i]['name']; ?>
              </td>
              <td>
                <input type="radio" name="id"
                value="<?php echo $files[$i]['id']; ?>">
              </td>
            </tr>
            <?php
            }
            ?>
        </tbody>
      </table>
      <div class="input-group">
        <input type="file" name="fileComp">
      </div>
      <div class="input-group">
        <input type="submit" value="Compare">
      </div>
    </form>
  </div>
<?php
}