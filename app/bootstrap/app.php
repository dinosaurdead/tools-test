<?php
spl_autoload_register(function ($className) {
	require_once  'app' . _DIR_ . 'config' ._DIR_ . 'config.class.php';
    require_once  'app' . _DIR_ . 'lib' ._DIR_ . 'connection' . _DIR_ . 'database' . _DIR_ . 'pdo.class.php';
    require_once  'app' . _DIR_ . 'lib' ._DIR_ . 'connection' . _DIR_ . 'database' . _DIR_ . 'pdo.factory.class.php';
    require_once  'app' . _DIR_ . 'models' ._DIR_ . 'file.class.php';
    require_once 'app' . _DIR_ . 'lib' ._DIR_ . 'connection' . _DIR_ . 'ssh' . _DIR_ . 'ssh.class.php';
	require_once 'app' . _DIR_ . 'lib' ._DIR_ . 'connection' . _DIR_ . 'ssh' . _DIR_ . 'ssh.factory.class.php';
	require_once 'app' . _DIR_ . 'lib' ._DIR_ . 'encrypt' . _DIR_ . 'aes.class.php';
});